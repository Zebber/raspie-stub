package com.cegeka.iot.sandwichdelivery.raspiestub.nats;

import io.nats.client.Connection;
import io.nats.client.Nats;
import io.nats.client.Options;

import javax.net.ssl.SSLContext;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by nathanv on 26/05/2017.
 */
public class NatsHandler {

    private static final String NATS_LOCATION = "nats://sensor:raspisensor@sandwichdelivery.cegeka.com:4222";

    private final Connection   connect;

    public NatsHandler(String natsLocation) {
        try {
            Options opts = new Options.Builder()
//                    .secure()
//                    .sslContext(SSLContext.getDefault())
                    .build();

            connect = Nats.connect(natsLocation, opts);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public NatsHandler() {
        this(NATS_LOCATION);
    }

    public <T> NatsChannel<T> createChannel(String subject, Function<T, byte[]> transformer) {
        return new NatsChannel<>(connect, subject, transformer);
    }

    public <T> void subscribe(String subject,
                          Function<byte[], T> transformer,
                          Consumer<T> consumer) {
        connect.subscribe(subject, msg -> {
            T transformedObject = transformer.apply(msg.getData());
            consumer.accept(transformedObject);
        });
    }

}
