package com.cegeka.iot.sandwichdelivery.raspiestub;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;

/**
 * Created by nathanv on 12/07/2017.
 */
public class SensorReadMessageMapper {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public SensorReadMessageMapper() {
        SerializationConfig serializationConfig = objectMapper.getSerializationConfig();

        serializationConfig.getDefaultVisibilityChecker()
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE);
    }

    public byte[] mapToBytes(SensorReadMessage sensorReadMessage) {
        try {
            return objectMapper.writeValueAsBytes(sensorReadMessage);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
