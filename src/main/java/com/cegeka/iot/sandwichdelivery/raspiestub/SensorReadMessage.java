package com.cegeka.iot.sandwichdelivery.raspiestub;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class SensorReadMessage {
    private double weight;
    private double temp;
    private long timestamp;

    @SuppressWarnings("unused")
    private SensorReadMessage() {
    }

    public SensorReadMessage(double weight,
                             double temp,
                             long timestamp) {
        this.weight = weight;
        this.temp = temp;
        this.timestamp = timestamp;
    }

    public double getWeight() {
        return weight;
    }

    public double getTemp() {
        return temp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public LocalDateTime asLocalDateTime() {
        return LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.UTC);
    }

    public String timestampAsFormattedString() {
        return DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).format(asLocalDateTime());
    }
}
