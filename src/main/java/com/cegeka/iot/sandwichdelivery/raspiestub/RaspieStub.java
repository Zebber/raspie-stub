package com.cegeka.iot.sandwichdelivery.raspiestub;

import com.cegeka.iot.sandwichdelivery.raspiestub.nats.NatsChannel;
import com.cegeka.iot.sandwichdelivery.raspiestub.nats.NatsHandler;

import java.util.Random;

/**
 * Created by nathanv on 12/07/2017.
 */
public class RaspieStub {

    public static void main(String[] args) throws Exception {
        NatsChannel<SensorReadMessage> channel = new NatsHandler("nats://localhost:4222")
                .createChannel(
                "event.publish.sensorRead", new SensorReadMessageMapper()::mapToBytes);

        Random random = new Random();

        while (true) {
            double weight = random.nextDouble() * 2.5 + 40.38;

            channel.publish(new SensorReadMessage(weight, 25.0, System.currentTimeMillis() / 1000));

            Thread.sleep(1000);
        }
    }
}
