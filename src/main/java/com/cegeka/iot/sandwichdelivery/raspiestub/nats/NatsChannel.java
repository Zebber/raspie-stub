package com.cegeka.iot.sandwichdelivery.raspiestub.nats;

import io.nats.client.Connection;

import java.io.IOException;
import java.util.function.Function;

/**
 * Created by nathanv on 12/07/2017.
 */
public class NatsChannel<T> {

    private final Connection connection;
    private final String subject;
    private final Function<T, byte[]> transformer;

    NatsChannel(Connection connection,
                String subject,
                Function<T, byte[]> transformer) {
        this.connection = connection;
        this.subject = subject;
        this.transformer = transformer;
    }

    public void publish(T message) {
        try {
            connection.publish(subject, transformer.apply(message));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
